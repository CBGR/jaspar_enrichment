# This must be run with R_LIBS referencing the appropriate directory.

required <- c("devtools", "BiocManager", "data.table", "dplyr",
              "ggplot2", "htmlwidgets", "optparse", "plotly",
              "RColorBrewer", "reshape2", "usethis")

# location <- .libPaths()[1]

# to_install <- setdiff(required, rownames(installed.packages(location)))

install.packages(required, repos="https://cran.uib.no/")

BiocManager::install(c("BiocGenerics", "S4Vectors", "IRanges", "GenomicRanges"))

# Install ggbeeswarm 0.6.0 because later versions give problems

library(devtools)
install_version("ggbeeswarm", version = "0.6.0", repos = "https://cran.uib.no/")