JASPAR Enrichment
=================

The JASPAR enrichment tool predicts which sets of TFBSs from the JASPAR_
database are enriched in a set of given genomic regions. Enrichment
computations are performed using the LOLA tool. For more information about the
underlying enrichment computations, read the `LOLA documentation`_.

The tool allows for two types of computations:

- Enrichment of TFBSs in a set of genomic regions compared to a given universe
  of genomic regions.

- Differential TFBS enrichment when comparing one set of genomic regions (S1)
  to another (S2).

.. _JASPAR: https://jaspar.genereg.net/
.. _LOLA: http://code.databio.org/LOLA/
.. _`LOLA documentation`: http://code.databio.org/LOLA/

Prerequisites
-------------

First of all, it is assumed that you will be attempting to use the software on
a Unix-compatible system with a POSIX-compatible shell. To retrieve and
administer this and other software, the ``git`` tool is required, typically
provided by the ``git`` system package.

A deployment script is provided that will perform the retrieval and
installation of system packages as well as programming language-specific
packages where appropriate. The system package details apply to the Fedora
GNU/Linux distribution, but other distributions will use similar package
names.

For more information about the software needed, see the `prerequisites`_
documentation.

.. _`prerequisites`: docs/Prerequisites.rst

Getting Started
---------------

To be able to use this software, you first need to download the software:

.. code:: shell

  git clone https://bitbucket.org/CBGR/jaspar_enrichment.git

Now, enter the ``jaspar_enrichment`` directory:

.. code:: shell

  cd jaspar_enrichment

Two options are available to ease the installation of all dependencies. Very 
briefly, dependencies can be obtained through our docker image:

.. code:: shell

  docker pull cbgr/jaspar_tfbs_enrichment:latest

Or by using the ``deploy.sh`` script in this repository as follows:

.. code:: shell

  ./deploy.sh -n -p commandline

In the latter, some activities will require elevated privileges obtained using 
``sudo``. Others will be run with normal privileges.

To perform a normal deployment involving some privileged activities to obtain
system packages:

.. code:: shell

  ./deploy.sh -p commandline

Run the script as follows to see more information:

.. code:: shell

  ./deploy.sh --help

.. _`here`: docs/Prerequisites.rst

Obtaining Necessary Data
------------------------

Several databases can be obtained from the latest Zenodo records for this
project:

https://doi.org/10.5281/zenodo.6860527

https://doi.org/10.5281/zenodo.6860555

These can be downloaded manually from the above Web page. Alternatively, a
program is provided to automate the downloading process for one or more
databases.

To see available databases for each record:

.. code:: shell

  bin/zenodo_fetch 5555932 -l
  bin/zenodo_fetch 5555937 -l

To download specific databases (such as those for Schizosaccharomyces pombe):

.. code:: shell

  bin/zenodo_fetch 5555932 -f sacCer3.tar.gz

To download all databases from each record:

.. code:: shell

  bin/zenodo_fetch 5555932
  bin/zenodo_fetch 5555937

For each organism and genome assembly, TFBS predictions are stored as
compressed files containing all batches. The source code that was used to
build these databases is freely available `here
<https://bitbucket.org/CBGR/jaspar_lola_db_creation>`_.

Each downloaded archive unpacks into a directory with the same basename. For
example, ``sacCer3.tar.gz`` produces a directory called ``sacCer3`` when
unpacked. It is this directory that will be provided to the tool as the LOLA
database directory.

Using the JASPAR Enrichment Tools
---------------------------------

A summary of the different operating modes of the tools is given below.

Enrichment within a given universe of genomic regions
+++++++++++++++++++++++++++++++++++++++++++++++++++++

.. image:: img/oneSetBg.png
   :alt: Enrichment with a background

To compute which sets of TFBSs from JASPAR are enriched in a set *S* of
genomic regions compared to a universe *U* of genomic regions, you can use the
``oneSetBg`` subcommand as follows.

.. code:: shell

   bin/JASPAR_enrich.sh oneSetBg <loladb_dir> <S bed> <U bed> <output dir> <JASPAR_RELEASE_YEAR> <n_cores>

Where:

- ``<loladb_dir>`` is the directory containing all the LOLA database batches
  for an organism and assembly.

- ``<JASPAR_RELEASE_YEAR>`` is the JASPAR release year whose TFBSs are being used.

- ``<n_cores>`` is the number of cores to use when parallelizing.

Alternatively, using the docker image requires launching the tool as follows when 
using docker:

.. code:: shell

  docker run \
    -it \
    -v $(realpath <bin_dir>):/bin/:rw \
    -v $(realpath <loladb_dir>):/LOLA_db/:rw \
    -v $(realpath <input_dir>):/inputs/:rw \
    -v $(realpath <output_dir>):/outputs/:rw \
    cbgr/jaspar_tfbs_enrichment:latest \
      bash bin/JASPAR_enrich.sh \
        oneSetBg \
        /LOLA_db/ \
        /inputs/<S bed> \
        /inputs/<U bed> \
        /outputs/ \
        <JASPAR_RELEASE_YEAR> \
        <n_cores>

or singularity:

.. code:: shell

  singularity run \
    -e \
    --home <root_dir> \
    -B $(realpath <loladb_dir>):/LOLA_db/,$(realpath <input_dir>):/inputs/,$(realpath <output_dir>):/outputs/ \
    docker://cbgr/jaspar_tfbs_enrichment:latest \
      bash bin/JASPAR_enrich.sh \
        oneSetBg \
        /LOLA_db/ \
        /inputs/<S bed> \
        /inputs/<U bed> \
        /outputs/ \
        <JASPAR_RELEASE_YEAR> \
        <n_cores>

Where ``<loladb_dir>``, ``<JASPAR_RELEASE_YEAR>``, and ``<n_cores>`` are the same as previously 
described and:

- ``<bin_dir>`` is the path to the bin directory in this repository.

- ``<root_dir>`` is this repository's root directory.

- ``<input_dir>`` is the directory containing <S bed> and <U bed>.

- ``<output_dir>`` is the path to the output directory. Please make sure this 
directory exists when using the docker image.


In any of the two cases, this will compute the enrichment of TFBS sets from 
JASPAR in the genomic regions from *S* (provided as a BED file) when compared to 
the expectation from a universe *U* of genomic regions (provided as a BED file). 
All result files will be provided in the ``<output dir>`` directory.  

.. admonition:: Note

  Every region in *S* should overlap with one region in *U*.

Differential enrichment
+++++++++++++++++++++++

.. image:: img/twoSets.png
   :alt: Differential enrichment

To compute which sets of TFBSs from JASPAR are enriched in a set *S1* of
genomic regions compared to another set *S2* of genomic regions, you can use
the ``twoSets`` subcommand as follows.

.. code:: shell

   bin/JASPAR_enrich.sh twoSets <loladb_dir> <S1 bed> <S2 bed> <output dir> <JASPAR_RELEASE_YEAR> <n_cores>

Here:  

- ``<loladb_dir>`` is the directory containing all the LOLA database batches
  for an organism and assembly.

- ``<JASPAR_RELEASE_YEAR>``  is the JASPAR release year whose TFBSs are being used.

- ``<n_cores>`` is the number of cores to use when parallelizing.

When running through our docker image, the tool should be run as follows when 
using docker:

.. code:: shell

  docker run \
    -it \
    -v $(realpath <bin_dir>):/bin/:rw \
    -v $(realpath <loladb_dir>):/LOLA_db/:rw \
    -v $(realpath <input_dir>):/inputs/:rw \
    -v $(realpath <output_dir>):/outputs/:rw \
    cbgr/jaspar_tfbs_enrichment:latest \
      bash bin/JASPAR_enrich.sh \
        twoSets \
        /LOLA_db/ \
        /inputs/<S1 bed> \
        /inputs/<S2 bed> \
        /outputs/ \
        <JASPAR_RELEASE_YEAR> \
        <n_cores>

or singularity:

.. code:: shell

  singularity run \
    -e \
    --home <root_dir> \
    -B $(realpath <loladb_dir>):/LOLA_db/,$(realpath <input_dir>):/inputs/,$(realpath <output_dir>):/outputs/ \
    docker://cbgr/jaspar_tfbs_enrichment:latest \
      bash bin/JASPAR_enrich.sh \
        twoSets \
        /LOLA_db/ \
        /inputs/<S1 bed> \
        /inputs/<S2 bed> \
        /outputs/ \
        <JASPAR_RELEASE_YEAR> \
        <n_cores>

Where ``<loladb_dir>``, ``<JASPAR_RELEASE_YEAR>``, and ``<n_cores>`` are the same as previously 
described and:

- ``<bin_dir>`` is the path to the bin directory in this repository.

- ``<root_dir>`` is this repository's root directory.

- ``<input_dir>`` is the directory containing <S bed> and <U bed>.

- ``<output_dir>`` is the path to the output directory. Please make sure this 
directory exists when using the docker image.

This will compute the enrichment of TFBS sets from JASPAR in the genomic
regions from *S1* (provided as a BED file) when compared to the genomic
regions in *S2* (provided as a BED file). All result files will be provided in
the ``<output dir>`` directory.

Output
++++++

The output directory will contain the ``allEnrichments.tsv`` file that
provides the enrichment score for each TFBS set from JASPAR along with their
metadata information. Similar files (following the template ``col_.tsv``) are
created for each TF with all data sets available for that TF.

A visual representation of the enrichment analysis is provided in the output
directory with three plots:

1. A swarm plot using the log10(p-value) of the enrichment for each TFBS
   set on the y-axis.

   File: ``allEnrichments_swarm.pdf``

2. An interactive `beeswarm plot`_ showing the 10 most enriched TFs.

   File: ``JASPAR_enrichment_interactive_beeswarmplot.html``

3. An interactive `ranking plot`_ showing the log10(p-value) for all the
   datasets in JASPAR.

   File: ``JASPAR_enrichment_interactive_ranking.html``

In the three plots, the data sets for the top 10 TFs showing a log10(p-value)
< 3 are highlighted with dedicated colors (one color per TF). Data sets with
log10(p-value) > 3 are provided with a color for N.S.  (non-significant).

Users can explore the results with the **interactive plots**. These plots
allow to hide/show the top 10 enriched TFs, and the tooltip displays the TF
name, log10(p-value) (also known as significance), and cell type and
treatment.

.. _`beeswarm plot`: data/example_Fleischer_et_al/JASPAR_enrichment_interactive_beeswarmplot.html
.. _`ranking plot`: data/example_Fleischer_et_al/JASPAR_enrichment_interactive_ranking.html

Example
+++++++

As an example of application, we provide data derived from the
publication `DNA methylation at enhancers identifies distinct breast
cancer lineages, Fleischer, Tekpli, et al, Nature Communications,
2017 <https://www.nature.com/articles/s41467-017-00510-x>`_. The
genomic regions of interest correspond to 200bp-long regions around CpGs
from cluster 2A described in the publication. These regions around CpGs
of interest are shown to be associated with FOXA1, GATA, and ESR1
binding. We applied the following command to compute TFBS enrichment
using all the CpG probes from the Illumina Infinium HumanMethylation450
microarray:

.. code:: shell

   bin/JASPAR_enrich.sh oneSetBg hg38 \
                                 data/example_Fleischer_et_al/clusterA_200bp_hg38.bed \
                                 data/example_Fleischer_et_al/450k_probes_hg38_200bp.bed \
                                 JASPAR_enrichment \
                                 2022 \
                                 10

Here, ``hg38`` is the directory containing the LOLA databases for hg38,
produced when unpacking the ``hg38.tar.gz`` archive. This directory is not
provided in this repository due to the large file sizes involved. The
necessary files can be found in Zenodo records as described above.

We observe a clear enrichment for TFBSs associated with the expected
TFs. The corresponding swarm plot is:

.. image:: data/example_Fleischer_et_al/allEnrichments_swarm.png
   :alt: Swarm plot

A complementary beeswarm plot allows to clearly visualize the 10 most
enriched TFs.

.. image:: data/example_Fleischer_et_al/JASPAR_enrichment_beeswarmplot.png
   :alt: Swarm plot showing the ten most enriched TFs

The enrichment and ranking of all the datasets can be visualized in the
ranking plot.

.. image:: data/example_Fleischer_et_al/JASPAR_enrichment_ranking.png
   :alt: Enrichment and ranking of all datasets
