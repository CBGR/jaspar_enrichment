FROM fedora:36

RUN mkdir /opt/enrichment

COPY requirements-sys.txt /opt/enrichment/requirements-sys.txt
COPY requirements.txt /opt/enrichment/requirements.txt
COPY requirements.R /opt/enrichment/requirements.R

WORKDIR /opt/enrichment

# Install system requirements

RUN xargs dnf -y install < "requirements-sys.txt"

# Install bedtools

RUN mkdir bedtools \
    && git clone https://github.com/arq5x/bedtools2.git bedtools
WORKDIR /opt/enrichment/bedtools
RUN make \
    && make install DESTDIR="/opt/enrichment/lib" \
    && ln -s /opt/enrichment/lib/usr/local/bin/bedtools /usr/local/bin/bedtools

# Install R packages

WORKDIR /opt/enrichment
RUN Rscript requirements.R

# Create user account without root privileges

RUN useradd -u 1001 user \
    && chown -R user /opt/enrichment/
USER user

# Install python packages

RUN python3 -m pip install --root /opt/enrichment/lib -r requirements.txt
ENV PYTHONPATH="/opt/enrichment/lib/usr/local/lib64/python3.10/site-packages/:/opt/enrichment/lib/usr/local/lib/python3.10/site-packages/"
RUN python3 -m pip install -r requirements.txt