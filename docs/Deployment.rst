Deployment
==========

The deployment of this application involves the installation and configuration
of software, mostly conducted using deployment scripts.

Some details of the individual scripts are given below.

System Package Installation
+++++++++++++++++++++++++++

The ``00-root-packages.sh`` script installs the packages listed in the
``requirements-sys.txt`` file. Some of these packages are actually needed to
build other kinds of packages, such as those provided by Python and R.

Other Package Installation
++++++++++++++++++++++++++

The ``01-user-dependencies.sh`` script installs non-system packages as an
unprivileged user in a directory local to the application. These packages are
listed in the following files:

- ``requirements.txt`` for Python packages
- ``requirements.R`` for R packages

BEDtools Installation
+++++++++++++++++++++

The ``02-user-bedtools.sh`` script builds the BEDtools_ software which is not
currently available as a system package.

.. _BEDtools: http://bedtools.readthedocs.io/en/latest/content/installation.html

Data Provision
++++++++++++++

The following only applies when the software is deployed to support a JASPAR
installation.

The ``05-user-static-data.sh`` script unpacks data archives and populates the
``data`` directory with databases used by the JASPAR Web site.

Permissions and SELinux Labelling
+++++++++++++++++++++++++++++++++

The following only applies when the software is deployed to support a JASPAR
installation.

The ``11-root-selinux-bin.sh`` script applies SELinux labelling to programs in
the ``bin`` directory so that a Web server may run them.

The ``11-root-selinux-lib.sh`` script labels the installed programs and
libraries in the ``lib`` directory retaining locally installed software.
