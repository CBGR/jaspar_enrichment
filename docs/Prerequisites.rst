Prerequisites
=============

Since the JASPAR Enrichment tool relies on other software, a number of prerequisite
packages and tools - the software dependencies - are required for this
software to function. Administering these can be fairly complicated, and it is 
recommended to follow one of the options described in the following sections to 
install the necessary dependencies.

Currently, it is possible to install the required dependencies in two different
ways. The first one, which we recommend due to its simplicity, is using the 
docker image available at our `docker hub`_. The second one, which we refer to as
manual installation, involves the usage of the deployment script (``deploy.sh``).
For more details of each of the deployment scripts in the manual installation, 
see the `deployment`_ scripts documentation.

.. _`docker_hub`: https://hub.docker.com/r/cbgr/jaspar_tfbs_enrichment
.. _`deployment`: Deployment.rst

Docker image
============

The simplest way to obtain all required software is through the image at our 
`docker hub`_ . Images in this repository are tagged according to their version. 
Additionally, the latest version is tagged with the :code:`latest` tag. 

Please refer to the `Docker official documentation`_ to find information on how to
install docker in your system. Once installed, the latest version can be pulled 
with: 

.. code:: shell

   docker pull cbgr/jaspar_tfbs_enrichment:latest

.. _`docker hub`: https://hub.docker.com/r/cbgr/jaspar_tfbs_enrichment
.. _`Docker official documentation`: https://docs.docker.com/

Manual installation
===================

Obtaining and installing the prerequisites involves the following steps:

1. Installing system packages to provide fundamental software
2. Installing language-specific packages to provide extra functionality
3. Building packages only available as source code

System packages should be available via your system's software package
manager. The package names are defined in the ``requirements-sys.txt`` file
and apply to the Fedora GNU/Linux distribution. Similar packages may be
available for other distributions.

Language-specific packages are provided by the Python and R software
repositories using the appropriate tools. The ``requirements.txt`` and
``requirements.R`` files describe the respective language-specific
requirements or prerequisites.

Some software is not available via the above channels and needs to be obtained
and built. The BEDtools_ software is an example of this, and a deployment
script is provided to perform the necessary operations to obtain, build and
deploy the software.

.. _BEDtools: http://bedtools.readthedocs.io/en/latest/content/installation.html

The installed software will reside in the ``lib`` directory created inside the
top level of this software distribution. The supplied programs are aware of
the ``lib`` directory and will attempt to find prerequisite programs and
libraries there if it is present.

Below, some notes are provided on various central software requirements
together with more details about how the above process affects them.

POSIX Shell
-----------

This software relies on the availability of a POSIX-compatible shell, which
includes ``bash`` (GNU Bash) and ``sh`` (the Bourne shell) on modern Unix-like
operating systems.

Python 3
--------

Note that Python 3 should be pre-installed in your operating system
distribution, or it should be installed if the initial deployment instructions
have been followed to install the fundamental prerequisites (Python, ``git``
and ``pip``). Check its availability by typing the following at a shell
prompt:

.. code:: shell

   whereis python3

If Python 3 is not available, or if you cannot, or do not wish to, install
system packages for it, you can install it from python.org_. After
installation, there are several required libraries that have to be installed.

This software has previously employed Python 2.7 but is now configured to use
Python 3, with Python 3.6 and 3.7 having been tested.

.. _python.org: https://www.python.org/downloads/

Library dependencies
++++++++++++++++++++

To be able to prepare swarm plots, a number of Python packages need to be
installed for the Python environment.

These can be installed using the ``pip3`` tool which may already be available
alongside other Python programs on your system. If not, you can install it via
your software package manager following the `pip tutorial`_.

With ``pip3``, the packages can be installed in the shell command environment
as follows:

.. code:: shell

  pip3 install -r requirements.txt

Or without the actual ``pip3`` command using ``python3``:

.. code:: shell

  python3 -m pip install -r requirements.txt

.. _`pip tutorial`: https://pip.pypa.io/en/stable/installing/

R Language Environment
----------------------

You can install the latest version of the R statistical environment from CRAN_
or by using the software package manager on your system. The minimum required
R version is 2.5.0.

If installing from source, the following system-level packages may need to be
installed:

::

  gcc-gfortran java-1.8.0-openjdk-devel libcurl-devel libX11-devel libXt-devel
  pcre-devel readline-devel

.. _CRAN: https://cran.r-project.org/

Library dependencies
++++++++++++++++++++

Various libraries and tools are required within the R environment to run the
software, and these may need to be built against lower-level libraries. The
following system-level packages may therefore need to be installed:

::

  cairo-devel libcurl-devel libxml2-devel openssl-devel pandoc xz-devel
  zlib-devel

(These package names being suitable for Red Hat or Fedora distributions.)

The following command can be run to install required software packages:

.. code:: shell

  Rscript requirements.R
