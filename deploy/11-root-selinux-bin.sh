#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# Label the wrapper scripts.

ENRICH="$BASEDIR/bin/JASPAR_enrich.sh"

semanage fcontext -a -t bin_t "$ENRICH"
restorecon "$ENRICH"
