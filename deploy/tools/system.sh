#!/bin/sh

# SPDX-FileCopyrightText: 2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

have()
{
    which "$1" > /dev/null 2>&1
}

system_is_debian()
{
    have apt
}

system_is_redhat()
{
    have dnf
}

apache_service_name()
{
    if system_is_debian ; then
        echo "apache2"
    else
        echo "httpd"
    fi
}

apache_user_name()
{
    if system_is_debian ; then
        echo "www-data"
    else
        echo "apache"
    fi
}

apache_site_directory()
{
    if system_is_debian ; then
        echo "/etc/apache2/sites-available"
    else
        echo "/etc/httpd/conf.d"
    fi
}

# vim: tabstop=4 expandtab shiftwidth=4
