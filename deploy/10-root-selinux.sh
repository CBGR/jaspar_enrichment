#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`
DATADIR="$BASEDIR/data"

# Initialise data directory contents.

for FILENAME in "$DATADIR/"* ; do

    # Set a label for SELinux, apply the labelling for SELinux.

    if [ -d "$FILENAME" ] ; then
        semanage fcontext -a -t httpd_sys_content_t "$FILENAME(/.*)?"
        restorecon -R "$FILENAME"
    else
        semanage fcontext -a -t httpd_sys_content_t "$FILENAME"
        restorecon "$FILENAME"
    fi
done
