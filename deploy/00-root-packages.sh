#!/bin/sh

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

. "$THISDIR/tools/system.sh"

# Install system dependencies.
#

if have dnf ; then
    xargs dnf -y install < "$BASEDIR/requirements-sys.txt"
elif have apt ; then
    DEBIAN_FRONTEND=noninteractive xargs apt -y install < "$BASEDIR/requirements-sys-debian.txt"
else
    echo "System type not recognised." 1>&2
    exit 1
fi
