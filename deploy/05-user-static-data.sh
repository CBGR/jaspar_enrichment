#!/bin/sh

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# Permit deployment of the latest files.

if [ "$1" = '--latest' ] ; then
    LATEST=$1
    shift 1
else
    LATEST=
fi

# Define a common storage location.

DATADIR="$BASEDIR/data"

# Find the source data.

SOURCEDIR="/var/tmp/jaspar-data"

if [ ! -e "$SOURCEDIR" ] ; then
    echo "JASPAR data directory missing: $SOURCEDIR" 1>&2
    exit 1
fi

# Unpack the data into the output directory.

for FILENAME in `find "$SOURCEDIR" -maxdepth 1 -name '*.tar*'` ; do

    # Only unpack the latest file of a given type.

    if [ "$LATEST" ] && ! "$THISDIR/tools/latest_file" "$FILENAME" > /dev/null ; then
        echo "Ignoring older file: $FILENAME" 1>&2
        continue
    fi

    # Identify the file type and tag.

    FILETYPE=`"$THISDIR/tools/file_type" "$FILENAME"`
    TAG=`"$THISDIR/tools/file_tag" "$FILENAME"`

    # Obtain a suitable distinguishing name based on the archive name.

    FILETYPENAME=`"$THISDIR/tools/file_type" "$FILENAME" --name`

    # Choose a suitable directory for unpacking.

    UNPACKTEST=

    if [ "$FILETYPE" = 'LOLA_databases' ] ; then
        UNPACKDIR="$DATADIR/$TAG/$FILETYPENAME"
    else
        echo "Ignoring unrecognised file: $FILENAME" 1>&2
        continue
    fi

    UNPACKTEST=${UNPACKTEST:-$UNPACKDIR}

    # Unpack the archive if apparently necessary.

    if [ "$FILENAME" -nt "$UNPACKTEST" ] ; then
        "$THISDIR/tools/tar_extract" "$FILENAME" "$UNPACKDIR"
        touch "$UNPACKTEST"
    fi
done
