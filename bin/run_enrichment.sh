lola_db=$1
tmpin=$2
tmpbg=$3
out_dir=$4

THISDIR=$(dirname $0)

batch_dir=${lola_db%".RDS"}
batch_dir=(${batch_dir//"_"/" "})
batch_dir=${batch_dir[-2]}"_"${batch_dir[-1]}
mkdir -p ${out_dir}/${batch_dir}/

R --silent --slave --vanilla -f "$THISDIR/lola.R" \
     --args "$lola_db" "$tmpin" "$tmpbg" "${out_dir}/${batch_dir}/" "$THISDIR/LOLA_modif"
