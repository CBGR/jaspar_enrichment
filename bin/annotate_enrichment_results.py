from pyjaspar import jaspardb
import pandas as pd
import argparse

## ---------------- ##
## Argument parsing ##
## ---------------- ##

parser = argparse.ArgumentParser(description = 'Annotate the results of an enrichment job using a specified JASPAR database.')
parser.add_argument('--input', help = 'Path to the input table with the results of an enrichment.')
parser.add_argument('--JASPAR_release', help = 'JASPAR release to use indicated as its year of release. E.g. 2022, 2020, etc.', type = str)
args = parser.parse_args()

input_path = args.input
release = args.JASPAR_release

## ---------------------------- ##
## Load relevant JASPAR release ##
## ---------------------------- ##

release_str = 'JASPAR' + release

jdb_obj = jaspardb(release=release_str)

## --------------- ##
## Read input file ##
## --------------- ##

input = pd.read_csv(input_path, sep = '\t', header = 0)

## ------------------------------------------------------------- ##
## Iterate over input file to obtain class and family annotation ##
## ------------------------------------------------------------- ##

class_annotations = []
family_annotations = []
matrixIDs = input['matrixID'].tolist()

for matrixID in matrixIDs:
    pyjaspar_entry = jdb_obj.fetch_motif_by_id(matrixID)
    class_annotations.append('::'.join(pyjaspar_entry.tf_class))
    family_annotations.append('::'.join(pyjaspar_entry.tf_family))

input['class'] = class_annotations
input['family'] = family_annotations

## -------------------------- ##
## Write output to same table ##
## -------------------------- ##

input.to_csv(input_path, sep = '\t', header = True, index = False)