#!/bin/sh

# SPDX-FileCopyrightText: 2019-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Define appropriate program and library locations for local software
# installations. Otherwise, use system software installations.

if [ -e "$LIB" ] ; then

    # Obtain the specific Python version in order to construct usable names
    # for programs and path components.

    PYTHON_PREFIX=`python3 -c 'import sys; print(sys.prefix)'`
    PYTHON_VERSION=`python3 -c 'import sys; print("%s.%s" % (sys.version_info.major, sys.version_info.minor))'`
    PYTHON="python${PYTHON_VERSION}"

    # Construct Python path permutations.

    PYTHONPATH=

    for LOCAL in "" "local/" ; do
        for LIBDIR in "lib" "lib64" ; do
            DIR="lib$PYTHON_PREFIX/${LOCAL}$LIBDIR/$PYTHON/site-packages"
            if [ "$PYTHONPATH" ] ; then
                PYTHONPATH="$PYTHONPATH:$DIR"
            else
                PYTHONPATH="$DIR"
            fi
        done
    done

    export PYTHONPATH
else
    PYTHON="python3"
fi

# vim: tabstop=4 expandtab shiftwidth=4
