#!/usr/bin/python
# *-* coding: utf-8 *-*

""" Create a swarm plot for the enrichment results obtained with LOLA. """

import sys
import getopt


def in_list(therow, tfset):
    if therow['family'] in tfset:
        return therow['family']
    elif therow['family'] == "N.S.":
        return "N.S."
    else:
        return "Other"


###############################################################################
#                               MAIN
###############################################################################
if __name__ == "__main__":
    usage = '''
    %s -f <input file> -o <output file>
    ''' % (sys.argv[0])

    try:
        opts, args = getopt.getopt(sys.argv[1:], "f:o:h")
    except getopt.GetoptError:
        sys.exit(str(getopt.GetoptError) + usage)

    infile = None
    outfile = None
    pvalue = False
    qvalue = False
    for o, a in opts:
        if o == '-f':
            infile = a
        elif o == '-o':
            outfile = a
        else:
            sys.exit(usage)
    if not(infile and outfile):
        sys.exit(usage)

    import pandas as pd
    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import numpy as np
    import seaborn as sns
    sns.set(font_scale=0.8)

    data = pd.read_csv(infile, sep="\t", header=0)
    data = data[data.userSet == 1]
    data.loc[(data['pValueLog'] < 2), 'family'] = "N.S."
    data['oddsRatio'] = np.log2(data['oddsRatio'] + 1.)
    data = data.reset_index(drop=True)
    tfs = set()
    for indx, row in data.iterrows():
        if row['family'] != "N.S.":
            tfs.add(row['family'])
        if len(tfs) > 9:
            break
    data['family'] = data.apply(lambda row: in_list(row, tfs), axis=1)
    fig = sns.swarmplot(y=data['pValueLog'], hue=data['family'],
                        x=[""] * len(data),
                        palette=sns.color_palette("Paired", 12), size=2)
    #fig = sns.stripplot(y=data['pValueLog'], hue=data['family'],
    #                    x=[""] * len(data),
    #                    palette=sns.color_palette("Paired", 12), size=2)
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.ylabel('-log$_{10}$(p-value)')
    plt.savefig(outfile, bbox_inches="tight")
    # generate png file also
    plt.savefig(outfile+".png", bbox_inches="tight")
