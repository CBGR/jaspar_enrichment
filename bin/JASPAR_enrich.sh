#!/bin/bash

# SPDX-FileCopyrightText: 2019-2021 University of Oslo
# SPDX-FileContributor: Rafael Riudavets Puig <r.r.puig@ncmm.uio.no>
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-FileContributor: jaimicore <jcastro@lcg.unam.mx>
# SPDX-FileContributor: Anthony Mathelier <anthony.mathelier@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

PROGNAME=$(basename $0)
THISDIR=$(dirname $0)

# Location of required libraries.

LIB=`realpath "$THISDIR/../lib"`

# Define appropriate program and library locations for local software
# installations. Otherwise, use system software installations.

. "$THISDIR/python_env.sh"

if [ -e "$LIB" ] ; then

    # Make locally installed R libraries available.

    PREFIX="$LIB/usr/local"
    export R_LIBS="$PREFIX/lib/R/library"

    # Make locally installed programs available.

    PATH="$PREFIX/bin:$PATH"
fi


# Plot charts for the output data.
# plot <output directory>

plot() {
    python3 $THISDIR"/swarm_plot.py" \
       -f "$1/allEnrichments.tsv" \
       -o "$1/allEnrichments_swarm.pdf"

    Rscript $THISDIR"/JASPAR_enrichment_swarmplot.R" \
        -t "$1/allEnrichments.tsv" \
        -o "$1"
}


# Subcommand functions.

sub_help() {
    echo "Usage: $PROGNAME <subcommand> [options]\n"
    echo "Subcommands:"
    echo "    oneSetBg   Apply one set enrichment analysis against a background"
    echo "    usage: $PROGNAME oneSetBg <loladb_dir> <bed file> <universe/background bed> <output dir> <JASPAR_RELEASE_YEAR> <n_cores>"
    echo "    twoSets  Apply two sets differential enrichment analysis"
    echo "    usage: $PROGNAME twoSets <loladb_dir> <1st bed file> <2nd bed file> <output dir> <JASPAR_RELEASE_YEAR> <n_cores>"
    echo ""
    echo "For help with each subcommand run:"
    echo "$PROGNAME <subcommand> -h|--help"
    echo ""
}

sub_oneSetBg() {
    echo "Running 'oneSetBg' analysis"
    tmpin=$(mktemp)
    bedtools sort -i "$2" | bedtools merge -i stdin > "$tmpin"
    tmpbg=$(mktemp)
    bedtools sort -i "$3" | bedtools merge -i stdin > "$tmpbg"

    printf "%s\n" "${LOLA_DBS[@]}" | xargs -I {} --max-procs=$PROCESSORS bash $THISDIR/run_enrichment.sh {} $tmpin $tmpbg $4

    if R --silent --slave --vanilla -f "$THISDIR/concatenate_batch_results.R" \
           --args "$4" "$4"; then

             STATUS=0
    else
             STATUS=$?
    fi

    python3 "$THISDIR/annotate_enrichment_results.py" \
           --input "$4/allEnrichments.tsv" --JASPAR_release "$5"

    plot "$4"

    rm $tmpin $tmpbg
    return $STATUS
}

sub_twoSets() {
    echo "Running 'twoSets' analysis"
    tmpone=$(mktemp)
    tmptwo=$(mktemp)
    bedtools subtract -a "$2" -b "$3" | bedtools sort -i stdin | \
        bedtools merge -i stdin > "$tmpone"
    bedtools subtract -b "$2" -a "$3" | bedtools sort -i stdin | \
        bedtools merge -i stdin > "$tmptwo"

    printf "%s\n" "${LOLA_DBS[@]}" | xargs -I {} --max-procs=$PROCESSORS bash $THISDIR/run_diff_enrichment.sh {} $tmpone $tmptwo $4

    if R --silent --slave --vanilla -f "$THISDIR/concatenate_batch_results.R" \
           --args "$4" "$4"; then

             STATUS=0
    else
             STATUS=$?
    fi

    python3 "$THISDIR/annotate_enrichment_results.py" \
           --input "$4/allEnrichments.tsv" --JASPAR_release "$5"

    plot "$4"

    rm $tmpone $tmptwo
    return $STATUS
}



# Main program.

# Interpret the subcommand.

SUBCOMMAND=$1
shift

case $SUBCOMMAND in

    # Help message.

    "" | "-h" | "--help")
        sub_help
        ;;

    # Operation subcommands.

    *)
        LOLA_DBS=$(find "$1" -name "*.RDS")

        if [ ${#LOLA_DBS[@]} -eq 0 ] ; then
            echo "No LOLA databases found in the input directory! Exiting..."
            exit 1
        fi

        if [ -z "$5" ] ; then
            $5="http://jaspar.genereg.net/api/v1/matrix/"
        fi

        if [ -z "$6" ] ; then
            PROCESSORS=1
        else
            PROCESSORS=$6
        fi

        echo "Number of processors: $PROCESSORS"

        # Make any output directory.

        if [ ! -e "$4" ] ; then
            mkdir -p "$4"
        fi

        # Invoke recognised subcommands.

        IFS=
        sub_${SUBCOMMAND} $@
        STATUS=$?

        # Upon success, write a special file indicating this condition, exiting
        # with the success status code.

        if [ $STATUS = 0 ] ; then
            touch "$4/success"
            exit 0
        fi

        # Upon failure, write a special file indicating this condition.

        touch "$4/failed"

        # The special status code of 127 indicates that the subcommand function
        # was not recognised.

        if [ $STATUS = 127 ] ; then
            echo "Error: '$SUBCOMMAND' is not a known subcommand." >&2
            echo "       Run '$PROGNAME --help' for a list of known subcommands." >&2
            exit 1
        fi

	exit $STATUS
        ;;
esac
